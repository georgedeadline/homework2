fun main(){

    val p1 = Point(3,-4)
    val p2 = Point(-8,4)
    val a = (p1.equals(p2))
    val b = p1 + p2
    println(-b)
    println(a)
    println(p1)
    println(p2)



}
data class Point(val x:Int, val y:Int) {

    operator fun plus(point: Point): Point{
        return Point(this.x + point.x,this.y + point.y)
    }
}

operator fun Point.unaryMinus() = Point(-x, -y)
//s